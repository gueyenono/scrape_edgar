source("scrape_rigpaf.R")
library(edgar)
library(purrr)
library(fs)

# Get edgar files
output <- getFilings(cik.no = c(1000180, 38079), c('10-K','10-Q'), 2006, quarter = c(1, 2, 3), downl.permit = "n")

# Create a folder where the scrape files will be saved
dir_create("SCRAPED_FILES")

# Locate Edgar text files
txt_files <- dir_ls(glob = "*.txt", recurse = TRUE)

# Scrape the desired portion of text: Report of Independent Registered Public Accounting Firm
map(txt_files, scrape_rigpaf, directory_path = "SCRAPED_FILES")
